package com.sistema.qa.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class nombreQA {

	@Id
	@Column(name = "id_nombre")
	private Integer id;

	private String titulo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public nombreQA(Integer id, String titulo) {
		super();
		this.id = id;
		this.titulo = titulo;
	}

	public nombreQA() {
		super();
	}

	public nombreQA(String titulo) {
		super();
		this.titulo = titulo;
	}

	public nombreQA(Integer id) {
		super();
		this.id = id;
	}

}

package com.sistema.qa.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sistema.qa.modelo.proyecto;

public interface PeliculaRepositorio extends JpaRepository<proyecto, Integer>{

}

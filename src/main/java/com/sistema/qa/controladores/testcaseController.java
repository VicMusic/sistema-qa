package com.sistema.qa.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class testcaseController {

    @GetMapping("/formTestCase")
    public String testcase(){
        return "formTestCase";
    }
    
}
